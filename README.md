# NSW sTGC Trigger Chain Test System 

A test system has been developed for NSW sTGC trigger chain composed of:

- Trigger chain test firmware based on minidaq board (custom back-end hardware).

- Trigger chain test control GUI. See the git repo [here](https://gitlab.cern.ch/xxiao/trigger-chain-debug-script) (Ask Xiong for permision).

This repo is aimed to provide a detail guideline for how to operate the system.

## Trigger Chain Test Control GUI
This GUI is used to control trigger chain test firmware by ethernet communication. It is used for both Reception test and on wedge trigger interface test. 

![](./pics/GUI_example.png)


## Reception test 

![](./pics/Diagram.png)
This test is the on bench test for trigger chain when we received FEBs. It is aimed to check the trigger interface in board level and VMM direct output in chip level. Detailed procdeures:

- pFEB:


    1.pFEB TDS PRBS bit error rate check: verify the functionality of TDS serializer.

    2.pFEB TOT pulse check using test pulse: check if there is any dead channel. 

- sFEB:


    1.sFEB TDS PRBS bit error rate check: verify the functionality of TDS serializer.

    2.sFEB TDS IDLE packet check: check if the inner logic of TDS was damaged.

    3.sFEB trigger receiving check: check if sFEB TDS could receive 640 Mbps trigger signal correctly.

    4.sFEB 6bADC linearity scan using test pulse: check the linearity of each channel and if there is any dead channel.

    5.sFEB TDS-VMM phase scan: get the phases that allow TDS to receive 6bADC data from VMM correctly. 


## On Wedge Trigger Interface check
Please see this [page](./on-wedge-link/on-wedge-link.md).

## Firmware Bit files
Please see the gitlab [here](https://gitlab.cern.ch/xxiao/nsw-trigger-bit-files).

There is a local cope on trigger chain test cart PC located at 
```
/home/nswstgc/work/nsw-trigger-bit-files

```
## Additional information 
1. The hostname of the current operating cart is tdaqpc.cern.ch or umichdesktopssun.cern.ch. Ask Xiong for account name and password. 

