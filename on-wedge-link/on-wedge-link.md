# On Wedge Trigger Interface Check

- This test is operated on the NSW STGC trigger chain test cart. 
- There are a full set of screen reording vidoes on how to operate the system avaiable at:
```
https://drive.google.com/drive/folders/1OmtGGDI5ZtZcTfPp_uTnJFOtPDDMisPV?usp=sharing
```
- Check those videos anytime if you encouter any problem.

## Preparation

- Move the  cart next to the wedge you are going to test. 
- Connect the power strip cable and one fiber on RimL1DDC to the related felix fiber pannel. 
- Power on the PC and select kernel 4.4 if you want wifi adapter available.

## 0.Get SCA ID mapping
- Power the whole wedge on
- Ssh to felix machine with stgcic account

Using felix1 as an examle
```
ssh1 or (ssh -Y stgcic@um-felix1.cern.ch) 
openFELIX_GUI
```

![](./pics/felix_control_gui.png)

- Click "Kill"
- Click "Turn on"
- Click "PingSCA FEB" 
- Save the sca id info at 
```
~/work/On_Wedge_test/reports/elink_mapping.txt
```

Here is an example of what it looks like:

``` 
all the sFEBs
16580-L1Q1
27437-L1Q2
22094-L1Q3

16602-L2Q1
22178-L2Q2
22199-L2Q3

16500-L3Q1
22172-L3Q2
27397-L3Q3

16606-L4Q1
22182-L4Q2
22288-L4Q3

all the pFEBs

16660-L1Q2
21295-L3Q2
21450-L1Q1
21274-L3Q1

20966-L2Q2
20890-L4Q2
21439-L2Q1
21446-L4Q1

16663-L1Q3
16595-L2Q3
16640-L3Q3
16593-L4Q3
```

## 1.sFEB strip-TDS PRBS

- Using Router board and router IBERT firmware. 
- Connect 3 sFEB of one layer router cables to Router board. In total of 4 times of this test is needed for all 4 layers. 
- Configure all FEB to prbs mode. Using json file:
```
 /afs/cern.ch/user/s/stgcic/public/benchtest/wedge/wedge_prbs_flx{1 or 2}.json 
```
- Power up Rim-L1DDC and Router board. Connect the Jtag cable of Router board to PC. 
- Open vivado
```
sv (or source /opt/Xilinx/Vivado/2016.2/settings64.sh )
vivado
```
- Program router IBERT firmware.
```
/home/nswstgc/work/nsw-trigger-bit-files/router_PRBS.bit
```
- Select RX-PRBS31 and check RX-polarity invert.

- Right click an empty space and "Export to execl" to save the reuslts after the bit error rate reached your requirement.

- Save the result for layer1 sFEBs as an example:
```
~/work/On_Wedge_test/reports/sFEB/L1.xlsx
```



## 2. sFEB strip-TDS trigger interface - Pad trigger input to sFEB


1. Connect sFEB cable to J13 connector and trigger cable to J9 connector. 
1. Power on mindaq2 board and RimL1DDC board.
1. Progmam minidaq2 board with trigger firmware:

    ```
    /home/nswstgc/work/nsw-trigger-bit-files/minidaq2_trigger_4chnl/Test_env_top.bit

    ```

1. Open control GUI:

    ```
    cd /home/nswstgc/work/On_Wedge_test/trigger-chain-debug-script
    sudo su
    ./main.py
    ```
1. [Setup mini-daq test system.](../mini-daq-trigger-setup/mini-daq-trigger-setup.md) (Open tshark and check the ethernet communication.)

1. Setup GUI:

    ![](./pics/gui-trigger.png)

    - Selet Strip on the top left combobox. Also make sure you are using the right Felix. 
    - Only check "Trigger only" in strip test tab.
    - Input the correct board info 0, board info 1. Note: for board info 0, the only thing the gui cares about its the vmm number, make sure you have right vmm number which is either 6 or 8. For board info 1, it should in a format of SCAID followed by wedge location e.x. 21111-L1Q1.

1. Click "Create test" and then "Start Test"

    The test right now contais two of the test - "TESET_PRBS" and "TEST_STRIP_TRIGGER".

1. Wait for the finish and remove the test.

![](./pics/trigger-result.png)


Logs are stored at 

```
~/work/On_Wedge_test/reports/sFEB/
```
- If the "TEST_STRIP_TRIGGER" test failed, it is recommend to use Roctdsphase check box and input a value. The default value is 88 at now. Try 80 for example.
- If the trigger cable is too short, please use the LVDS6 repeater as an extension hub. Using CH1-IN and CH1-OUT ports and CH1-IN should be connected to minidaq2.

## 3.1pFEB pad-TDS PRBS

- Using Pad trigger board and pad trigger IBERT firmware. 
- Connect all 3 pFEB cables to pad trigger board. (Those 3 pFEB cables contain all the pFEB links)
- Configure all FEB to prbs mode. Using json file:
```
 /afs/cern.ch/user/s/stgcic/public/benchtest/wedge/wedge_prbs_flx{1 or 2}.json 
```
- Power up Rim-L1DDC and Pad trigger board. Connect the Jtag cable of Pad trigger board to PC. 
- Open vivado
- Program pad trigger IBERT firmware.
```
/home/nswstgc/work/nsw-trigger-bit-files/Padtrigger_prbs_12chnl_up.bit
```
- Select RX-PRBS31 and check RX-polarity invert.

- Right click an empty space and "Export to execl" to save the reuslts after the bit error rate reached your requirement.

- Save the results to: 
```
~/work/On_Wedge_test/reports/pFEB/PRBS.xlsx
```

## 3.2 pFEB mapping check

- Using minidaq2
- Connect one of the three pFEB cables to minidaq2 J13 port. 
- Configure all FEBs with following json file:
```
 /afs/cern.ch/user/s/stgcic/public/benchtest/wedge/pFEB_mapping_flx{1 or 2}.json 
```
- Power up Rim-L1DDC and minidaq2. 
- Open vivado
- Program pad trigger IBERT firmware.
```
/home/nswstgc/work/nsw-trigger-bit-files/minidaq2_trigger_4chnl/Test_env_top.bit
```

- [Setup mini-daq test system.](../mini-daq-trigger-setup/mini-daq-trigger-setup.md) (Open tshark and check the ethernet communication.)

![](./pics/gui_debug_tab.png)
- Switch to debug tab
- Click "Initialize"
- Click "PAD TOT"
- Click "Test once"
- The result should be shown like this:

![](./pics/tot_mapping.png)
Note:This is a special mapping configuration json file.
```
 The channel 0 TDS should has channel 0-31  having hits. 
 The channel 1 TDS should has channel 32-63 having hits. 
 The channel 2 TDS should has channel 64-95 having hits. 
 The channel 3 TDS should has channel 96-103 having hits. 
```
- If the result shows not as expected, use "stgc-dcs" gui to config json file and config TTC (LTP). The TTC .dat file store at: (Ask Xu for help)
```
/afs/cern.ch/user/s/stgcic/public/benchtest/pg_stgc_benchtest.dat
```

## 4. TOT noise - S-curve scan 

- Using minidaq2
- Connect one of the three pFEB cables to minidaq2 J13 port. 
- Power up Rim-L1DDC and minidaq2. 
- Open vivado
- Program tot noise test firmware
```
/home/nswstgc/work/nsw-trigger-bit-files/minidaq2_tot_1119/Test_env_top.bit
```

- [Setup mini-daq test system.](../mini-daq-trigger-setup/mini-daq-trigger-setup.md) (Open tshark and check the ethernet communication.)


1. Create threshold json. (Ask Xu for help on this step.)

    After readout chain finish the baseline trimmer scan, they should have a file with the final result. Make a copy of this baseline file to:
    ```
    ~stgcic/public/benchtest/baseline/
    ```

    An example name:
    ```
    ~stgcic/public/benchtest/baseline/trimmers_x30_Wedge6.json

    ```
    Change the ROC "CTRL Phase" to 2 on TDS ePLL tab and double check all the TDS channel has been enebled.

2. Open control GUI:

    ```
    cd /home/nswstgc/work/On_Wedge_test/trigger-chain-debug-script
    sudo su 
    python main.py

    ```

3. Setup GUI:
    ![](./pics/tot_setup.png)

    - Swithc to "Pad mode" on the top left checkbox. Also make sure you are using the right Felix.
    - Select "Confirm" or "Pivot" on the TOT tab on Wedge type.
    - Select "Large" or "small" on the TOT tab on Wedge size.
    - Check the "ToT noise enable" box 
    - Record the related board number  
    ![](./pics/tot_info_fill.png)

4. Try to use "0" and "50" with "config" button to config in TOT tab. Click "Enable" button in TOT tab for channel enable. This is a preview for the noise level. You shouldn't see any noise on "50" configuration and a lot of noise on "0" configuration.

5. Click Create test and Start Test

6. Wait for the finish and remove the test.

7. Check the result in folder:
    ```
    ~/work/On_Wedge_test/reports/TOT_NOISE/results
    ```
    - you will see the s-curve scan plot plus a summary .csv file for each board.
8. Click the "Whole wedge summary" button for a report generation of the entire wedge.
    ```
    ~/work/On_Wedge_test/reports/TOT_NOISE/results/summary_whole_wedge.csv
    ~/work/On_Wedge_test/reports/TOT_NOISE/results/MTFID_sTGC_TrigRO.csv
    ```
    - Rename the MTFID_sTGC_TrigRO.csv file with replacing the "MTFID" to the real MTFID. This is the data file for database.

## 5 Glue pFEB sata connector 

- Repeat the step in 3.1
- Keep prbs test running while glueing pFEB sata connector  
- Wait for few minutes and keep an eye on the prbs result. In case there is any broke link, you have to fix it before glue dried. 

## 6 Upload data to eos
- Make a copy of all reports to local "final_data_store" folder with MTFID appended.
```
cp -r ~/work/On_Wedge_test/reports ~/work/On_Wedge_test/final_data_store/Wedge8_20MNIWSAC00004 (change the wedge name accordingly)
```
- Upload to eos with raw data file  
```
scp -r  Wedge8_20MNIWSAC00004 stgcic@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/det-nsw-stgc/trigger_chain_test/On_wedge_trigger_chain_link/
```
- Upload the "MTFID_sTGC_TrigRO.csv" (replace the MTFID) file to location:
```
/eos/atlas/atlascerngroupdisk/det-nsw-stgc/ELX_DB_resources/Wedge_Test_Reports/TrigRO/
```
## 7 Fill checklist
- This test is located at "2.6 Trigger Interface Test" on the checklist
- Check all the checkbox if the test has been passed. 
- Fill the s-curve scan problem channel according to the "summary_whole_wedge.csv" file.
- all the data are stored at:
```
/eos/atlas/atlascerngroupdisk/det-nsw-stgc/trigger_chain_test/On_wedge_trigger_chain_link/YOURWEDGEFOLDERNAME
```




