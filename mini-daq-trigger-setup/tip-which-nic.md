

# Tip to figure out whcich is the correct nic name 

![](./pics/wireshark0.png)

Choose one possible nic and start monitoring. 


In the Vivado VIO, there is a vio named fake_data_sent_inst/sent_one_VIO. 
It will send a ethernet packet to pc if you toogle it. 

![](./pics/tip_which_nic1.png)



Check on the wireshark to see if you see one packet everytime if you toggle the button. 

![](./pics/tip_which_nic0.png)





**Note: vio is disabled once you click the "Initialize" button on the GUI. You need to click "debug_use_vio" button to enable vio.**