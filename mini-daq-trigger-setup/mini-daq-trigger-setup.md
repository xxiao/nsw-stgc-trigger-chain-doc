# Setup 

## Hardware connection
![RIM-L1DDC](./pics/RIM-L1DDC.JPG)
![SMA-clock](./pics/SMA-clock.JPG)
![KC705](./pics/KC705.JPG)
J9 - Trigger
J13 - TDS-data

## Software


1. Program related trigger bit file.

3. Configure control GUI

    ```
    cd /home/nswstgc/work/On_Wedge_test/trigger-chain-debug-script
    sudo su
    ./main.py
    ```


![](./pics/gui_general_setup.png)

    - Switch to "Setup" tab and click "Start tshark" then click "Ethernet inquiry"



![](./pics/received-packet.png)

    - You are expect to receive one packet as shown above
     




Note: the current NIC name is "em1" which is the ethernet card to receive raw ethernet packet from FPGA. If you are deploying a new PC or changing ethernet port you have to change the NIC name accordingly.o
open common.py and update:

```
nic='enp0s20f0u4'
```
