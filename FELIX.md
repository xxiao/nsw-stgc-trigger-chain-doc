# FELIX related command

Many useful commands are in ~/public/setupFELIX.sh

##  Restart FELIX

```
vivado -mode batch -nojournal -nolog -source ~xuwa/public/FLXfirmware/program_felix_script.tcl                                                                                                   
sudo systemctl reboot -i
sudo /etc/init.d/drivers_flx start  #Mount Driver
flx-init  #align GBTl
```
## Open stgc-dcs gui
```
setupGUI or source ~rowang/public/FELIX/setup_nsw_board_ttc.sh
stgc-dcs
```

## Useful command
```
flx-info GBT 
elinkconfig
ping_sca --hostname um-felix1 --from_host_port 12340 --to_host_port 12350 --elinks 3f,7f,bf,ff,13f,17f,1bf,1ff
/afs/cern.ch/work/r/rowang/public/FELIX/ScaSoftware/build/Demonstrators/PingSca/ping_sca --hostname um-felix1 --from_host_port 12341 --to_host_port 12350 --elinks 0,1,2,80
Note: from_host_port 12341 means flx card 2

```

## How to scan baseline and trimmer 
```
/usr/bin/eosfusebind
cd /afs/cern.ch/work/s/stgcic/public/sTGC_quick_and_dirty_baselines
source setup.sh
source /afs/cern.ch/work/s/stgcic/public/sTGC_quick_and_dirty_baselines/NSWConfiguration/dev/stgc_threshold_calib_cosmic.sh S P
```
