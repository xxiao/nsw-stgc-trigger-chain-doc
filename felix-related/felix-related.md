# FELIX related command 

## FELIX control GUI
How to open the felix control GUI
```
ssh -Y stgcic@um-felix1
openFELIX_GUI
```

![](./FELIX_CONTROL_GUI.png)

Button usage 
```
1. Turn On:
Will turn on felixcore and opc server if you checked the checkbox on the left side.
Note: you may need to manually selected the .xml file for opc server.
2. Kill 
Will kill all the felixcore and opc server opened by stgcic account.
3. PingSCA FEB
Check the SCA ID of each board on the wedge.
4. Elink Config
Config elink to allow basic FEB configuation. Note: no data readout ports are open.
5.Temp Config:
Using pre-defined json file and configuring all on Wedge FEBs twice to allow temperate monitoring.
Note: it may not work since the board typed mounted on the wedge may vary each time (sFEB6 vs sFEB8)
6. Config json File
Configure the json file you provide on the above.
```
